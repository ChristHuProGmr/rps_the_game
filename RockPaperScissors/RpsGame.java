package RockPaperScissors;
import java.util.Random;
//Christopher Hu . 1435688 
public class RpsGame{
    static final String ROCK       = "rock";
    static final String PAPER      = "paper";
    static final String SCISSORS   = "scissors";
    private int wins;
    private int losses;
    private int ties;
    Random rng;

    public RpsGame(){
        this.wins   = 0;
        this.losses = 0;
        this.ties   = 0;
        this.rng    = new Random();
    }

    public int getWins(){
        return wins;
    }

    public int getLosses(){
        return losses;
    }

    public int getTies(){
        return ties;
    }

    public String playRound(String p1Hand){
        final int MAX_SHAPES = 3;
        String p2Hand = "";
        String outcome = "";
        p1Hand = p1Hand.toLowerCase();

        if(chkValidHand(p1Hand)){
            p2Hand = assignP2Hand(this.rng.nextInt(MAX_SHAPES));

            outcome = showHands(p1Hand, p2Hand);
            return outcome;
        }
        else{
            throw new IllegalArgumentException("invalid user input");
        }
    }

    /**
     * validates if the player input is valid
     * @param p1Hand
     * @return
     */
    private boolean chkValidHand(String p1Hand){
        return (p1Hand.equals(ROCK) || p1Hand.equals(PAPER) || p1Hand.equals(SCISSORS));
    }

    private String assignP2Hand(int p2RNG){
        switch(p2RNG){
            case 0: return ROCK;
            case 1: return PAPER;
            case 2: return SCISSORS;
            default: throw new IllegalArgumentException();
        }
    }

    private String showHands(String p1Hand, String p2Hand){
        //In event of a tie
        if(p1Hand.equals(p2Hand)){
            incrementTies();
            return "Player 2 played " + p2Hand + ", it's a tie!";
        }
        //In event of a loss
        switch(p1Hand){
            case "rock":        if(p2Hand.equals(PAPER)){
                                    return procedureOnLoss(p2Hand);
                                }
                                break;

            case "paper":       if(p2Hand.equals(SCISSORS)){
                                    return procedureOnLoss(p2Hand);
                                }
                                break;

            case "scissors":    if(p2Hand.equals(ROCK)){
                                    return procedureOnLoss(p2Hand);
                                }
                                break;
        }
        //In event of a victory
        incrementWins();
        return "Player 2 played " + p2Hand + ", you win!";
    }

    private String procedureOnLoss(String p2Hand){
        incrementLosses();
        return "Player 2 played " + p2Hand + ", you lose!";
    }

    private void incrementWins(){
        this.wins++;
    }

    private void incrementLosses(){
        this.losses++;
    }

    private void incrementTies(){
        this.ties++;
    }
}