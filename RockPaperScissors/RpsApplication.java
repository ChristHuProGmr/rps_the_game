package RockPaperScissors;
//Christopher Hu . 1435688
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;


public class RpsApplication extends Application {	
	public void start(Stage stage) {
		Group root = new Group();
        RpsGame game = new RpsGame();

        //declaring new buttons
        Button rockBtn = new Button("Rock");
        Button paperBtn = new Button("Paper");
        Button scissorsBtn = new Button("Scissors");


        //declaring new boxes
        HBox hbox1 = new HBox();
        HBox hbox2 = new HBox();
        VBox vbox = new VBox();
        
        //declaring new Text objects
        Text status = new Text("Ahoy!");
        Text wins = new Text("Wins: 0");
        Text losses = new Text("Losses: 0");
        Text ties = new Text("Ties: 0");

        //create RpsChocie instances
        RpsChoice playRock = new RpsChoice(status, wins, losses, ties, "rock", game);
        RpsChoice playPaper = new RpsChoice(status, wins, losses, ties, "paper", game);
        RpsChoice playScissors = new RpsChoice(status, wins, losses, ties, "scissors", game);

        //set event handlers to buttons
        rockBtn.setOnAction(playRock);
        paperBtn.setOnAction(playPaper);
        scissorsBtn.setOnAction(playScissors);

        //add things to containers
        hbox1.getChildren().addAll(rockBtn, paperBtn, scissorsBtn);
        hbox2.getChildren().addAll(wins, losses, ties);
        vbox.getChildren().addAll(hbox1, status, hbox2);
        root.getChildren().add(vbox);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 200, 150); 
		scene.setFill(Color.PINK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

