package RockPaperScissors;
//Christopher Hu . 1435688
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;

public class RpsChoice implements EventHandler<ActionEvent>{
    private Text status;
    private Text wins;
    private Text losses;
    private Text ties;
    private String playerHand;
    private RpsGame game;

    public RpsChoice(Text status, Text wins, Text losses, Text ties, String playerHand, RpsGame game){
        this.status = status;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.playerHand = playerHand;
        this.game = game;
    }

    public void handle(ActionEvent e){
        String outcome = this.game.playRound(this.playerHand);
        this.status.setText(outcome);
        this.wins.setText("Wins: " + this.game.getWins());
        this.losses.setText("Losses: " + this.game.getLosses());
        this.ties.setText("Ties: " + this.game.getTies());
    }
}
